require 'nokogiri'
require 'open-uri'

module Debian
  @@pkg_in_new = []

  def Debian.fetch_packages_in_new!
    if @@pkg_in_new.empty?
      data = Nokogiri.HTML(open("https://ftp-master.debian.org/new.html"))
      data.css("td.package").each do |entry|
        @@pkg_in_new << entry.content
      end
    end
  end

  def Debian.show_packages_in_new
    Debian.fetch_packages_in_new!
    return @@pkg_in_new
  end
end
